var compositeCamerasIcon = L.icon({
  iconUrl: 'dist/images/icon.png',
  iconSize: [0, 0],
  iconAnchor: [0, 0],
  labelAnchor: [-6, 0]
});

/* Gray icons for non-specific cameras and guards. */
var camIcon = L.icon({
  iconUrl: 'dist/images/cam.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var fixedIcon = L.icon({
  iconUrl: 'dist/images/fixed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var panningIcon = L.icon({
  iconUrl: 'dist/images/panning.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var domeIcon = L.icon({
  iconUrl: 'dist/images/dome.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var guardIcon = L.icon({
  iconUrl: 'dist/images/guard.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var trafficIcon = L.icon({
  iconUrl: 'dist/images/traffic.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Blue icons for outdoor cameras and guards.
    Those cameras and guards surveil only private, i.e. non-public areas. */
var camBlueIcon = L.icon({
  iconUrl: 'dist/images/camBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var fixedBlueIcon = L.icon({
  iconUrl: 'dist/images/fixedBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var panningBlueIcon = L.icon({
  iconUrl: 'dist/images/panningBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var domeBlueIcon = L.icon({
  iconUrl: 'dist/images/domeBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var guardBlueIcon = L.icon({
  iconUrl: 'dist/images/guardBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Green icons for indoor cameras and guards. */
var camGreenIcon = L.icon({
  iconUrl: 'dist/images/camGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var fixedGreenIcon = L.icon({
  iconUrl: 'dist/images/fixedGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var panningGreenIcon = L.icon({
  iconUrl: 'dist/images/panningGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var domeGreenIcon = L.icon({
  iconUrl: 'dist/images/domeGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var guardGreenIcon = L.icon({
  iconUrl: 'dist/images/guardGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Red icons for outdoor cameras and guards.
    Those cameras and guards surveil public, i.e. non-private areas. */
var camRedIcon = L.icon({
  iconUrl: 'dist/images/camRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var fixedRedIcon = L.icon({
  iconUrl: 'dist/images/fixedRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var panningRedIcon = L.icon({
  iconUrl: 'dist/images/panningRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var domeRedIcon = L.icon({
  iconUrl: 'dist/images/domeRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var guardRedIcon = L.icon({
  iconUrl: 'dist/images/guardRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Gray-yellow icons for non-specific cameras and guards marked with a 'fixme' key. */
var todo_camIcon = L.icon({
  iconUrl: 'dist/images/todo_cam.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_fixedIcon = L.icon({
  iconUrl: 'dist/images/todo_fixed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_panningIcon = L.icon({
  iconUrl: 'dist/images/todo_panning.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_domeIcon = L.icon({
  iconUrl: 'dist/images/todo_dome.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_guardIcon = L.icon({
  iconUrl: 'dist/images/todo_guard.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_trafficIcon = L.icon({
  iconUrl: 'dist/images/todo_traffic.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Blue-yellow icons for outdoor cameras and guards marked with a 'fixme' key.
    Those cameras and guards surveil only private, i.e. non-public areas. */
var todo_camBlueIcon = L.icon({
  iconUrl: 'dist/images/todo_camBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_fixedBlueIcon = L.icon({
  iconUrl: 'dist/images/todo_fixedBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_panningBlueIcon = L.icon({
  iconUrl: 'dist/images/todo_panningBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_domeBlueIcon = L.icon({
  iconUrl: 'dist/images/todo_domeBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_guardBlueIcon = L.icon({
  iconUrl: 'dist/images/todo_guardBlue.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Green-yellow icons for indoor cameras and guards marked with a 'fixme' key. */
var todo_camGreenIcon = L.icon({
  iconUrl: 'dist/images/todo_camGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_fixedGreenIcon = L.icon({
  iconUrl: 'dist/images/todo_fixedGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_panningGreenIcon = L.icon({
  iconUrl: 'dist/images/todo_panningGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_domeGreenIcon = L.icon({
  iconUrl: 'dist/images/todo_domeGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_guardGreenIcon = L.icon({
  iconUrl: 'dist/images/todo_guardGreen.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

/* Red-yellow icons for outdoor cameras and guards marked with a 'fixme' key.
    Those cameras and guards surveil public, i.e. non-private areas. */
var todo_camRedIcon = L.icon({
  iconUrl: 'dist/images/todo_camRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_fixedRedIcon = L.icon({
  iconUrl: 'dist/images/todo_fixedRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_panningRedIcon = L.icon({
  iconUrl: 'dist/images/todo_panningRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_domeRedIcon = L.icon({
  iconUrl: 'dist/images/todo_domeRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});

var todo_guardRedIcon = L.icon({
  iconUrl: 'dist/images/todo_guardRed.png',
  iconSize: [20, 20],
  iconAnchor: [10, 10],
  popupAnchor : [0, -10]
});
